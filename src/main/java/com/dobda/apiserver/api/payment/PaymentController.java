package com.dobda.apiserver.api.payment;

import com.dobda.apiserver.api.common.errorhandling.BaseException;
import com.dobda.apiserver.api.payment.dto.MyAccountsInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class PaymentController {
	
	@Autowired
	private PaymentService paymentService;
	
	// 사용자 계좌 조회 //
	@GetMapping("/api/user/payment/account")
	public ResponseEntity account(Authentication authentication) {
		try {
            MyAccountsInfo result = paymentService.getMyAccountsInfo((Long)authentication.getPrincipal());
            return new ResponseEntity(result, HttpStatus.OK);
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
	}

//    // 계좌 입금 //
//    @PostMapping("/api/member/payment/deposit")
//    public ResponseEntity deposit(@RequestParam("adminToken") String accessToken,
//                                  @RequestParam("orderId") BigInteger orderId) {
//        try {
//            return new ResponseEntity(paymentService.requestDeposit(accessToken, orderId), HttpStatus.OK);
//        } catch (BaseException e) {
//            return new ResponseEntity(e.getMessage(), e.getStatusCode());
//        }
//    }
//
//    // 환불 진행 //
//    @PostMapping("/api/member/payment/refund")
//    public ResponseEntity refund(@RequestParam("adminToken") String accessToken,
//                                 @RequestParam("orderId") BigInteger orderId) {
//        try {
//            return new ResponseEntity(paymentService.requestRefund(accessToken, orderId), HttpStatus.OK);
//        } catch (BaseException e) {
//            return new ResponseEntity(e.getMessage(), e.getStatusCode());
//        }
//    }
}
