package com.dobda.apiserver.api.chat;

import com.dobda.apiserver.api.chat.entity.ChattingMsg;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;

public interface ChattingMsgRepository extends JpaRepository<ChattingMsg, BigInteger> {

    public Page<ChattingMsg> findByChattingRoom_Id(BigInteger roomId, Pageable pageable);

    public Page<ChattingMsg> findByIdLessThanAndChattingRoom_Id(BigInteger offsetId, BigInteger roomId, Pageable pageable);
}
