package com.dobda.apiserver.api.chat.dto;

import com.dobda.apiserver.api.chat.entity.ChattingMsg;
import lombok.Builder;
import lombok.Data;

import java.math.BigInteger;
import java.time.ZonedDateTime;

@Data
@Builder
public class ChattingMsgInfoDto {
    private BigInteger id;
    private BigInteger roomId;
    private Long senderId;
    private String message;
    private ZonedDateTime regTime;

    public static ChattingMsgInfoDto fromEntity(ChattingMsg entity) {
        if (entity == null) {
            return null;
        }
        return builder()
                .id(entity.getId())
                .roomId(entity.getChattingRoom().getId())
                .senderId(entity.getMemberUser().getId())
                .message(entity.getMessage())
                .regTime(entity.getRegTime())
                .build();
    }
}
