package com.dobda.apiserver.api.chat;

import com.dobda.apiserver.api.chat.dto.ChattingRoomInfoDto;
import com.dobda.apiserver.api.chat.dto.CreateChattingRoomDto;
import com.dobda.apiserver.api.chat.dto.ChattingMsgInfoDto;
import com.dobda.apiserver.api.chat.dto.SendChattingMsgDto;
import com.dobda.apiserver.api.chat.entity.ChattingMsg;
import com.dobda.apiserver.api.chat.entity.ChattingRoom;
import com.dobda.apiserver.api.chat.entity.ChattingRoomMember;
import com.dobda.apiserver.api.chat.entity.ChattingRoomMemberKey;
import com.dobda.apiserver.api.common.errorhandling.BadRequestException;
import com.dobda.apiserver.api.common.errorhandling.NotFoundException;
import com.dobda.apiserver.api.member.user.MemberUserRepository;
import com.dobda.apiserver.api.member.user.entity.MemberUser;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ChattingService {

    private final ChattingRoomRepository roomRepository;
    private final ChattingRoomMemberRepository roomMemberRepository;
    private final ChattingMsgRepository msgRepository;
    private final MemberUserRepository memberRepository;

    @Transactional
    public ChattingRoomInfoDto createRoom(Long memberId, CreateChattingRoomDto data) {
        ChattingRoomMember result = getRoom(memberId, data.getPartnerId());
        return ChattingRoomInfoDto.fromEntity(result);
    }

    public Page<ChattingRoomInfoDto> getChattingRoomList(Long memberId, Pageable pageable) {
        Page<ChattingRoomMember> findResults = roomMemberRepository.findByMemberUser_Id(memberId, pageable);

        return new PageImpl(
                findResults.getContent().stream()
                        .map(ChattingRoomInfoDto::fromEntity)
                        .collect(Collectors.toList()),
                pageable, findResults.getTotalElements());
    }

    @Transactional
    public void checkRead(Long memberId, BigInteger roomId) {
        Optional<ChattingRoomMember> findResult = roomMemberRepository.findById(new ChattingRoomMemberKey(roomId, memberId));
        if(findResult.isEmpty()) {
            throw new NotFoundException("항목을 찾을 수 없습니다.");
        }
        ChattingRoomMember roomMember = findResult.get();
        roomMember.setHasUnreadMsg(false);
    }

    @Transactional
    public ChattingMsgInfoDto sendMsg(Long memberId, SendChattingMsgDto data) {
        ChattingRoomMember myRoomMember = getRoom(memberId, data.getReceiverId());
        ChattingRoomMember receiverRoomMember = roomMemberRepository.findByMemberUser_IdAndPartner_Id(data.getReceiverId(), memberId);

        myRoomMember.setHasUnreadMsg(false);
        receiverRoomMember.setHasUnreadMsg(true);

        ChattingMsg newMsg = ChattingMsg.builder()
                .chattingRoom(myRoomMember.getChattingRoom())
                .memberUser(myRoomMember.getMemberUser())
                .message(data.getMessage())
                .build();
        newMsg.setRegTime(ZonedDateTime.now());

        myRoomMember.getChattingRoom().setLastMessage(data.getMessage());
        myRoomMember.getChattingRoom().setLastTime(newMsg.getRegTime());

        msgRepository.save(newMsg);

        return ChattingMsgInfoDto.fromEntity(newMsg);
    }

    @Transactional
    private ChattingRoomMember getRoom(Long memberId, Long partnerId) {
        if(memberId.equals(partnerId)) {
            throw new BadRequestException("채팅의 파트너가 자신일 수 없습니다.");
        }
        ChattingRoomMember roomMember = roomMemberRepository.findByMemberUser_IdAndPartner_Id(memberId, partnerId);

        if(roomMember != null) {
            return roomMember;
        }

        MemberUser me = memberRepository.findById(memberId).get();
        MemberUser partner = memberRepository.findById(partnerId).get();

        ChattingRoom newRoom = new ChattingRoom();
        roomRepository.save(newRoom);

        ChattingRoomMember myRoomMember = ChattingRoomMember.builder()
                .chattingRoom(newRoom)
                .memberUser(me)
                .partner(partner)
                .build();
        ChattingRoomMember partnerRoomMember = ChattingRoomMember.builder()
                .chattingRoom(newRoom)
                .memberUser(partner)
                .partner(me)
                .build();
        roomMemberRepository.save(myRoomMember);
        roomMemberRepository.save(partnerRoomMember);

        return myRoomMember;
    }

    @Transactional
    public Page<ChattingMsgInfoDto> getChattingMsgList(Long memberId, BigInteger roomId, BigInteger offsetId) {
        Optional<ChattingRoomMember> findRoom = roomMemberRepository.findById(new ChattingRoomMemberKey(roomId, memberId));
        if(findRoom.isEmpty()) {
            throw new NotFoundException("방을 찾을 수 없습니다.");
        }
        findRoom.get().setHasUnreadMsg(false);

        Pageable pageable = PageRequest.of(0, 20, Sort.Direction.DESC, "id");
        Page<ChattingMsg> findResults = null;
        if(offsetId == null || offsetId.equals("")) {
            findResults = msgRepository.findByChattingRoom_Id(roomId, pageable);
        } else {
            findResults = msgRepository.findByIdLessThanAndChattingRoom_Id(offsetId, roomId, pageable);
        }

        return new PageImpl(
                findResults.getContent().stream()
                        .map(ChattingMsgInfoDto::fromEntity)
                        .collect(Collectors.toList()),
                pageable, findResults.getTotalElements());
    }
}
