package com.dobda.apiserver.api.order;

import com.dobda.apiserver.api.order.entity.Order;
import com.dobda.apiserver.common.enums.OrderState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

// JPA를 사용하면서 CRUD를 선언하지 않고 사용하기 위해 JpaRepository를 상속받아서 사용
public interface OrderRepository extends JpaRepository<Order, BigInteger>{

	// 특정 하나의 데이터를 이용해서 DB값을 불러올 때 쿼리를 작성할 필요 없이 findby--를 이용해서 작성하면 된다
	// 만약 조건이 붙은 특정 데이터를 이용하기 위해서는 그에 맞는 쿼리를 추가해서 선언하면 된다.
    Page<Order> findByMemberUser_IdAndStateIn(Long memberId, Collection<OrderState> states, Pageable pageable);

    Page<Order> findByMemberHelper_IdAndStateIn(Long helperId, Collection<OrderState> states, Pageable pageable);

    List<Order> findAllByMemberUser_IdAndStateIn(Long memberId, Collection<OrderState> states);

    List<Order> findAllByMemberHelper_IdAndStateIn(Long helperId, Collection<OrderState> states);

    List<Order> findAllByMemberUser_IdAndState(Long memberId, OrderState state);

    List<Order> findAllByMemberHelper_IdAndState(Long helperId, OrderState state);
}
