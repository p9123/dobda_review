package com.dobda.apiserver.api.order.dto;

import com.dobda.apiserver.api.member.user.dto.MemberPublicInfo;
import com.dobda.apiserver.api.order.entity.Order;
import com.dobda.apiserver.api.requestboard.dto.RequestPostListInfoDto;
import com.dobda.apiserver.api.review.dto.ViewReviewDto;
import com.dobda.apiserver.common.enums.OrderState;
import com.dobda.apiserver.common.enums.PaymentMethod;
import lombok.Builder;
import lombok.Data;

import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class OrderInfoDTO {

	private BigInteger id;
    private RequestPostListInfoDto requestInfo;
	private MemberPublicInfo helperInfo;
	private PaymentMethod payMethod;
	private Long amount;
	private ZonedDateTime regTime;
	private OrderState state;
    private List<ViewReviewDto> reviews;
	
    // DB에 저장된 데이터를 불러오는 함수
    // Getter/Setter 대신 Builder를 사용한 이유는 필요한 데이터만 설정할 수 있고 코드가 좀 더 깔끔해지기 때문이다.
	public static OrderInfoDTO fromEntity(Order entity) {
		if(entity == null) {
			return null;
		}
		return builder()
                .id(entity.getId())
                .requestInfo(RequestPostListInfoDto.fromEntity(entity.getRequest()))
				.helperInfo(entity.getMemberHelper() != null
                        ? MemberPublicInfo.fromEntity(entity.getMemberHelper().getMemberUser())
                        : null)
				.payMethod(entity.getPayMethod())
				.amount(entity.getAmount())
				.regTime(entity.getRegTime())
				.state(entity.getState())
                .reviews(entity.getReviewList() != null
                        ? entity.getReviewList().stream().map(ViewReviewDto::fromEntity).collect(Collectors.toList())
                        : null)
				.build();
	}
}
