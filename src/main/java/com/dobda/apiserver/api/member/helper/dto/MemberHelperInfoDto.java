package com.dobda.apiserver.api.member.helper.dto;

import com.dobda.apiserver.api.member.helper.entity.MemberHelper;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@Builder
public class MemberHelperInfoDto {
	
	private Long helperId;
	private String address;
	//총 의뢰 접수 횟수
	private Long totalHelpCnt;
	//총 의뢰 완료 횟수
	private Long totalCompleteCnt;
	//총 의뢰 성공 횟수
	private Long totalSuccessCnt;
    private ZonedDateTime regTime;
	
	public static MemberHelperInfoDto fromEntity(MemberHelper entity) {
        if(entity == null) {
            return null;
        }
        return builder()
                .helperId(entity.getMemberUser().getId())
                .address(entity.getAddress())
                .totalHelpCnt(entity.getTotalHelpCnt())
                .totalCompleteCnt(entity.getTotalCompleteCnt())
                .totalSuccessCnt(entity.getTotalSuccessCnt())
                .regTime(entity.getRegTime())
                .build();
	}
	
}
