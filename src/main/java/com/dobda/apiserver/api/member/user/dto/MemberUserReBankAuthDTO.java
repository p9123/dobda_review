package com.dobda.apiserver.api.member.user.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class MemberUserReBankAuthDTO {
    
    @NotNull
    private String bankAuthCode;

    @NotNull
    private String bankCallbackUri;

    @NotNull
    private String pwd;
}
