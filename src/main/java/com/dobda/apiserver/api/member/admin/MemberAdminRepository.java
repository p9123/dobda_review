package com.dobda.apiserver.api.member.admin;

import com.dobda.apiserver.api.member.admin.entity.MemberAdmin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberAdminRepository extends JpaRepository<MemberAdmin, Long> {

	public MemberAdmin findByEmail(String email);
	
	public MemberAdmin findByNickname(String nickname);

    public MemberAdmin findByPhoneNum(String phoneNum);
    
}
