package com.dobda.apiserver.api.member.admin.dto;

import com.dobda.apiserver.api.member.admin.entity.MemberAdmin;
import com.dobda.apiserver.common.enums.AdminLevel;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@Builder
public class MemberAdminDetailInfo {
    private Boolean isAdmin = true;
    private Long id;
    private String email;
    private String name;
    private String nickname;
    private String phoneNum;
    private Boolean isMaster;
    private Integer unreadNotiCount;
    private ZonedDateTime regTime;

    public static MemberAdminDetailInfo fromEntity(MemberAdmin entity) {
        if (entity == null) {
            return null;
        }
        return builder()
                .isAdmin(true)
                .id(entity.getId())
                .email(entity.getEmail())
                .name(entity.getName())
                .nickname(entity.getNickname())
                .phoneNum(entity.getPhoneNum())
                .isMaster(entity.getLevel() == AdminLevel.MASTER)
                .unreadNotiCount(entity.getUnreadNotiCount())
                .regTime(entity.getRegTime())
                .build();
    }
}
