package com.dobda.apiserver.api.member.helper.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class MemberHelperApplyDTO {
	
	@NotNull
	private String address;

    @NotNull
	private MultipartFile idCard;

    @NotNull
	private String bankName;		//계좌주 이름 

    @NotNull
	private String fintechNum;
}
