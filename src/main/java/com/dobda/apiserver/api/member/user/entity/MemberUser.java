package com.dobda.apiserver.api.member.user.entity;

import com.dobda.apiserver.api.chat.entity.ChattingMsg;
import com.dobda.apiserver.api.chat.entity.ChattingRoomMember;
import com.dobda.apiserver.api.member.helper.entity.HelperApply;
import com.dobda.apiserver.api.member.helper.entity.MemberHelper;
import com.dobda.apiserver.api.order.entity.Order;
import com.dobda.apiserver.api.requestboard.entity.Request;
import com.dobda.apiserver.api.review.entity.Review;
import com.dobda.apiserver.common.enums.MemberUserLevel;
import com.dobda.apiserver.common.enums.RequestState;
import com.dobda.apiserver.security.MemberRoles;
import com.dobda.apiserver.security.model.CommonMemberSecureInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Getter @Setter
@DynamicInsert
@DynamicUpdate
@Table(name="member")
@NoArgsConstructor
public class MemberUser extends CommonMemberSecureInfo{

	@Size(min=2, max=50)
	@Column(nullable = false)
	private String name;

    @Size(min=3, max=50)
    @Column(unique = true, nullable = false)
    private String nickname;
	
	@Size(max=20)
	@Column(unique = true, nullable = false)
	private String phoneNum;
	
	@Column(columnDefinition = "TINYINT UNSIGNED", nullable = false)
	private MemberUserLevel level = MemberUserLevel.USER;						//일반회원:10 헬퍼:40
	
	@Size(max = 200)
	private String introduction; 				//자기소개글
	
	@Column(columnDefinition = "TINYINT UNSIGNED DEFAULT 0", nullable = false)
	private Integer unreadNotiCount;				//읽지 않은 알람 수

    @Size(max=400)
    @Column(nullable = false)
    private String bankToken;

    @Size(max=400)
    @Column(nullable = false)
    private String bankRefreshToken;

    @Column(columnDefinition = "DATETIME", nullable = false)
    private ZonedDateTime bankTokenExpireTime;

    @Size(max=10)
    @Column(nullable = false)
    private String bankUserCode;
	
	@Column(columnDefinition = "DATETIME")
	private ZonedDateTime denyTime;						//제재시간(언제까지)
	
	@Column(columnDefinition = "DOUBLE UNSIGNED DEFAULT 36.5", nullable = false)
	private Double trustPoint;					//신뢰도(기본 36.5) 		//default 값 넣어주기 위해 객체 타입으로 저장(DynamicInsert)
	
	@Column(columnDefinition = "INT UNSIGNED DEFAULT 0", nullable = false)
	private Long totalRequestCount;			//총 의뢰 맡긴 횟수
	
	@Column(columnDefinition = "INT UNSIGNED DEFAULT 0", nullable = false)
	private Long totalReceivedReportCount; 	//총 신고 받은 횟수
	
    @Builder
    public MemberUser(String email, byte[] pwd, String name, String nickname, String phoneNum, String introduction,
                      String bankToken, String bankRefreshToken, ZonedDateTime bankTokenExpireTime, String bankUserCode) {
        super(email, pwd);
        this.name = name;
        this.nickname = nickname;
        this.phoneNum = phoneNum;
        this.introduction = introduction;
        this.bankToken = bankToken;
        this.bankRefreshToken = bankRefreshToken;
        this.bankTokenExpireTime = bankTokenExpireTime;
        this.bankUserCode = bankUserCode;
    }

    @OneToMany(mappedBy = "memberUser", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<NotificationMember> notificationList = new ArrayList<>();

    //헬퍼 테이블
    @OneToOne(mappedBy="memberUser", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private MemberHelper memberHelper;

    @OneToOne(mappedBy="memberUser", optional = false, fetch = FetchType.LAZY)
    private HelperApply helperApply;

    @OneToMany(mappedBy = "memberUser")
    private List<Request> requestList = new ArrayList<>();

    @OneToMany(mappedBy = "memberUser")
    private List<Order> orderList = new ArrayList<>();

    @OneToMany(mappedBy = "writer")
    private List<Review> reviewFromMeList = new ArrayList<>();

    @OneToMany(mappedBy = "target", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Review> reviewToMeList = new ArrayList<>();

    @OneToMany(mappedBy = "memberUser")
    private List<ChattingMsg> chattingMsgList = new ArrayList<>();

    @OneToMany(mappedBy = "memberUser", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ChattingRoomMember> chattingRoomList = new ArrayList<>();

    @OneToMany(mappedBy = "partner")
    private List<ChattingRoomMember> participatingChattingRoomList = new ArrayList<>();

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(MemberRoles.USER.getFullRole()));
        if(level == MemberUserLevel.HELPER) {
            authorities.add(new SimpleGrantedAuthority(MemberUserLevel.HELPER.toString()));
        }
		return authorities;
	}

    @PreRemove
    private void preRemove() {
        reviewFromMeList.forEach(v -> v.setWriter(null));
        reviewFromMeList.clear();
        orderList.forEach(v -> v.setMemberUser(null));
        orderList.clear();
        requestList.forEach(v -> {
            v.setState(RequestState.DELETED);
            v.setMemberUser(null);
        });
        requestList.clear();
        chattingMsgList.forEach(v -> v.setMemberUser(null));
        chattingMsgList.clear();
        participatingChattingRoomList.forEach(v -> v.setPartner(null));
        participatingChattingRoomList.clear();
    }
}
