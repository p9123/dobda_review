package com.dobda.apiserver.api.member.user.dto;

import com.dobda.apiserver.api.member.user.entity.NotificationMember;
import com.dobda.apiserver.common.enums.ResourceType;
import lombok.Builder;
import lombok.Data;

import java.math.BigInteger;
import java.time.ZonedDateTime;

@Data
@Builder
public class NotificationMemberInfoDto {
    private BigInteger id;
    private Long memberId;
    private String message;
    private ResourceType targetType;
    private BigInteger targetId;
    private ZonedDateTime regTime;

    public static NotificationMemberInfoDto fromEntity(NotificationMember entity) {
        if (entity == null) {
            return null;
        }
        return builder()
                .id(entity.getId())
                .memberId(entity.getMemberUser().getId())
                .message(entity.getMessage())
                .targetType(entity.getTargetType())
                .targetId(entity.getTargetId())
                .regTime(entity.getRegTime())
                .build();
    }
}
