package com.dobda.apiserver.common.util.openbank.payment;

import lombok.Data;

@Data
public class TokenResponse {

	private String access_token;
	private String token_type;
	private String expires_in;
	private String scope;
	private String client_use_code;
	
}
