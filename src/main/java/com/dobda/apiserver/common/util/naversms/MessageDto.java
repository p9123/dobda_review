package com.dobda.apiserver.common.util.naversms;

import lombok.*;

@AllArgsConstructor
@Data
@Builder
public class MessageDto {
    private String to;
    private String content;
}