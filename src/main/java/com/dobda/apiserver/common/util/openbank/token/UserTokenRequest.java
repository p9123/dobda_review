package com.dobda.apiserver.common.util.openbank.token;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserTokenRequest {

    private String code;
    private String redirectUri;
}
