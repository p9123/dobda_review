package com.dobda.apiserver.common.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class RequestStateConverter implements AttributeConverter<RequestState, Integer> {
    @Override
    public Integer convertToDatabaseColumn(RequestState enumValue) {
        if (enumValue == null) {
            return null;
        }
        return enumValue.getCode();
    }

    @Override
    public RequestState convertToEntityAttribute(Integer integer) {
        if(integer == null) {
            return null;
        }
        return RequestState.fromCode(integer);
    }
}
