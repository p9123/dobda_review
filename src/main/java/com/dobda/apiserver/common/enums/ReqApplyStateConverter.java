package com.dobda.apiserver.common.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ReqApplyStateConverter implements AttributeConverter<ReqAplyState, Integer>{

	@Override
	public Integer convertToDatabaseColumn(ReqAplyState state) {
		if(state == null) {
			return null;			
		}
		return state.getCode();
	}

	@Override
	public ReqAplyState convertToEntityAttribute(Integer code) {
		if(code == null) {
			return null;
		}
		return ReqAplyState.fromCode(code);
	}
}
