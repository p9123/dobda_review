package com.dobda.apiserver.security.config;

import com.dobda.apiserver.security.MemberRoles;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class TopicSubscriptionInterceptor implements ChannelInterceptor {

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(message);
        if (StompCommand.SUBSCRIBE.equals(headerAccessor.getCommand())) {
            Authentication userPrincipal = (Authentication) headerAccessor.getUser();
            if (!validateSubscription(userPrincipal, headerAccessor.getDestination())) {
                throw new IllegalArgumentException("No permission for this topic");
            }
        }
        return message;
    }

    private boolean validateSubscription(Authentication authentication, String topicDestination) {
        if (authentication == null) {
            return false;
        }

        Long id = (Long) authentication.getPrincipal();
        String role = authentication.getAuthorities().stream()
                .filter(auth -> auth.getAuthority().startsWith("ROLE_"))
                .findAny().get().getAuthority();

        if(role.equals(MemberRoles.USER.getFullRole()) &&
            topicDestination.startsWith("/ws/topic/" + id)) {
            return true;
        }
        if(role.equals(MemberRoles.ADMIN.getFullRole()) &&
            topicDestination.startsWith("/ws/topic/admin/" + id)) {
            return true;
        }
        return false;
    }
}